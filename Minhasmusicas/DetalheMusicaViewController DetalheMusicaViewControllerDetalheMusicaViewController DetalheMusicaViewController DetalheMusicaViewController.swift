//
//  DetalheMusicaViewController DetalheMusicaViewControllerDetalheMusicaViewController DetalheMusicaViewController DetalheMusicaViewController.swift
//  Minhasmusicas
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class DetalheMusicaViewController_DetalheMusicaViewControllerDetalheMusicaViewController_DetalheMusicaViewController_DetalheMusicaViewController: UITableViewCell {
    

    
    
    var nomeMusica: String = ""
    var nomeAlbum: String = ""
    var nomeCantor: String = ""
    var nomeImagemPequena: String = ""
    var nomeImagemGrande: String = ""
    
    
    
    
    @IBOutlet weak var capa: UIImageView!
    @IBOutlet weak var musica: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var cantor: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.musica.text = self.nomeMusica
        self.album.text = self.nomeAlbum
        self.cantor.text = self.nomeCantor
        self.capa.image = UIImage(named: self.nomeImagemPequena)
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
